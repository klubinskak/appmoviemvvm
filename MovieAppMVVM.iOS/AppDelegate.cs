﻿using Foundation;
using MovieAppMVVM.Core;
using MvvmCross.Platforms.Ios.Core;
using UIKit;

namespace MovieAppMVVM.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the
    // User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
    [Register("AppDelegate")]
    public class AppDelegate : MvxApplicationDelegate<MvxIosSetup<App>, App>
    {

        public override UIWindow Window { get; set; }

        public bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            var result = base.FinishedLaunching(application, launchOptions);
            return result;
        }
    }
}

