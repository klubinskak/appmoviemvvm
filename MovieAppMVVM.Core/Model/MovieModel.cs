﻿using System;
using MvvmCross.ViewModels;

namespace MovieAppMVVM.Core.Model
{
    public class MovieModel : MvxViewModel
    {
        public string Title { get; set; }
        public string Date { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public string Director { get; set; }
        public string[] Actors { get; set; }
        public string[] MovieName { get; set; }
        public string[] ActorsImage { get; set; }
    }
}
