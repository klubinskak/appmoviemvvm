﻿using System;
using MovieAppMVVM.Core.Service;
using MovieAppMVVM.Core.ViewModel;
using MvvmCross;
using MvvmCross.ViewModels;

namespace MovieAppMVVM.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            Mvx.IoCProvider.RegisterType<IAddMovies, AddMovies>();
            RegisterAppStart<MoviesViewModel>();
        }
    }
}
