﻿using System;
using System.Windows.Input;
using MovieAppMVVM.Core.Model;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;


namespace MovieAppMVVM.Core.ViewModel
{
    public class MovieViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _navigationService = Mvx.IoCProvider.Resolve<MvxNavigationService>();
        

        public MovieViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        private string _title;
        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                RaisePropertyChanged(() => Title);


            }
        }
        private string _date;
        public string Date
        {
            get => _date;
            set
            {
                _date = value;
                RaisePropertyChanged(() => Date);
            }
        }
        private string _image;
        public string Image
        {
            get => _image;
            set
            {
                _date = value;
                RaisePropertyChanged(() => Image);
            }
        }
        private string _description;
        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                RaisePropertyChanged(() => Description);
            }
        }
        private MvxObservableCollection<MovieModel> _movies;
        public MvxObservableCollection<MovieModel> Movies
        {
            get => _movies;
            set
            {
                _movies = value;
                RaisePropertyChanged(() => Movies);
            }
        }
        //private MvxCommand<DetailsViewModel> _itemClickCommand;
        //public MvxCommand<DetailsViewModel> ItemClickCommand
        //{
        //    get
        //    {
        //        //_itemClickCommand = _itemClickCommand ?? new MvxCommand<DetailsViewModel>(OnClickCommand);
        //        //return _itemClickCommand;
        //    }
        //}
        //private void OnClickCommand(object item)
        //{
        //    //ShowViewModel<DetailsViewModel>();
        //    //Mvx.Resolve<IMvxNavigationService>().Navigate<DetailsViewModel>();
        //    //Mvx.IoCProvider.Resolve<IMvxNavigationService>().Navigate<DetailsViewModel>();
        //    //ShowViewModel<DetailsViewModel>(item);
        //}
        private MovieModel _selectedMovie;
        public MovieModel SelectedMovie
        {
            get { return _selectedMovie; }
            set
            {
                _selectedMovie = value;
                RaisePropertyChanged(() => SelectedMovie);

            }
        }
        //private MvxCommand<DetailsViewModel> _itemClickCommand;
        //public MvxCommand<DetailsViewModel> ItemClickCommand;
        //private void OnClickCommand(DetailsViewModel detailsViewModel)
        //{
        //    ItemClickCommand = _navigationService.Navigate<DetailsViewModel>();

        //}
        private IMvxAsyncCommand _navigateCommand;
        public IMvxAsyncCommand NavigateCommand
        {
            get
            {
                _navigateCommand = _navigateCommand ?? new MvxAsyncCommand(() => _navigationService.Navigate<DetailsViewModel>());
                return _navigateCommand;
            }
        }
    }
}
