﻿using System;
using System.Collections.Generic;
using MovieAppMVVM.Core.Model;
using MovieAppMVVM.Core.Service;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace MovieAppMVVM.Core.ViewModel
{
    public class MoviesViewModel : MvxViewModel
    {
        private List<MovieModel> _movies;
        private readonly IAddMovies _addMovies = Mvx.IoCProvider.Resolve<IAddMovies>();

        public MoviesViewModel()
        {
            Movies = _addMovies.GetMovies();

        }

        public List<MovieModel> Movies
        {
            get => _movies;
            set
            {
                _movies = value;
                RaisePropertyChanged(() => Movies);
            }
        }

    }
}

