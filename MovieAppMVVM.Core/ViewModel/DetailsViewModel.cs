﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using MovieAppMVVM.Core.Model;
using MovieAppMVVM.Core.Service;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace MovieAppMVVM.Core.ViewModel
{
    public class DetailsViewModel : MvxViewModel
    {
        private List<MovieModel> _movies;

        private readonly IAddMovies _addMovies = Mvx.IoCProvider.Resolve<IAddMovies>();

        private int _movieId;
        public void Init(int movieId)
        {
            _movieId = movieId;
        }

        public DetailsViewModel()
        {
            Movies = _addMovies.GetMovies();


        }
        public List<MovieModel> Movies
        {
            get => _movies;
            set
            {
                _movies = value;
                RaisePropertyChanged(() => Movies);
            }
        }
        private string _title;
        public string Title
        {
            get => _title;
            set
            {
                _title = value;
                RaisePropertyChanged(() => Title);


            }
        }
        private string _date;
        public string Date
        {
            get => _date;
            set
            {
                _date = value;
                RaisePropertyChanged(() => Date);
            }
        }
        private string _image;
        public string Image
        {
            get => _image;
            set
            {
                _date = value;
                RaisePropertyChanged(() => Image);
            }
        }
        private string _description;
        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                RaisePropertyChanged(() => Description);
            }
        }
        private string _director;
        public string Director
        {
            get => _director;
            set
            {
                _director = value;
                RaisePropertyChanged(() => Director);
            }
        }
        private string[] _actors;
        public string[] Actors
        {
            get => _actors;
            set
            {
                _actors = value;
                RaisePropertyChanged(() => Actors);
            }
        }
        private string[] _movieName;
        public string[] MovieName
        {
            get => _movieName;
            set
            {
                _movieName = value;
                RaisePropertyChanged(() => MovieName);
            }
        }
        private string[] _actorsImage;
        public string[] ActorsImage
        {
            get => _actorsImage;
            set
            {
                _actorsImage = value;
                RaisePropertyChanged(() => ActorsImage);
            }
        }
       


    }
}
