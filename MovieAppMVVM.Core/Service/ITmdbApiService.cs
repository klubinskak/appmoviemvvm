﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MovieAppMVVM.Core.Model;

namespace MovieAppMVVM.Core.Service
{
    public interface ITmdbApiService
    {
        Task<IEnumerable<TmdbMovieModel>> GetApiMovies();
    }
}
