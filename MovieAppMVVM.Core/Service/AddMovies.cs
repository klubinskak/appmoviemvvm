﻿using System;
using System.Collections.Generic;
using MovieAppMVVM.Core.Model;

namespace MovieAppMVVM.Core.Service
{
    public class AddMovies : IAddMovies
    {
        public List<MovieModel> GetMovies()
        {
            List<MovieModel> mMovies = new List<MovieModel>();
            mMovies.Add(new MovieModel() { Title = "Euphoria", Date = "2020", Image = "https://tiny.pl/9n2gf", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Director="Augustine Frizzell", Actors= new string[] { "Zendaya", "Hunter Schafer", "Angus Cloud" }, MovieName= new string[] { "Rue", "Jules", "Fezco" }, ActorsImage= new string[] { "https://tiny.pl/9n2jh", "https://tiny.pl/9n2jm", "https://tiny.pl/9n2pn" } });
            mMovies.Add(new MovieModel() { Title = "Games of thrones", Date = "2011", Image = "https://tiny.pl/9n2rg", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Director = "Augustine Frizzell", Actors = new string[] { "Zendaya", "Hunter Schafer", "Angus Cloud" }, MovieName = new string[] { "Rue", "Jules", "Fezco" }, ActorsImage = new string[] { "https://tiny.pl/9n2jh", "https://tiny.pl/9n2jm", "https://tiny.pl/9n2pn" } });
            mMovies.Add(new MovieModel() { Title = "Wolf of Wall Street", Date = "2013", Image = "https://fwcdn.pl/fpo/65/97/426597/7586610.3.jpg", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Director = "Augustine Frizzell", Actors = new string[] { "Zendaya", "Hunter Schafer", "Angus Cloud" }, MovieName = new string[] { "Rue", "Jules", "Fezco" }, ActorsImage = new string[] { "https://tiny.pl/9n2jh", "https://tiny.pl/9n2jm", "https://tiny.pl/9n2pn" } });
            mMovies.Add(new MovieModel() { Title = "Harry Potter", Date = "2002", Image = "https://tiny.pl/9n272", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Director = "Augustine Frizzell", Actors = new string[] { "Zendaya", "Hunter Schafer", "Angus Cloud" }, MovieName = new string[] { "Rue", "Jules", "Fezco" }, ActorsImage = new string[] { "https://tiny.pl/9n2jh", "https://tiny.pl/9n2jm", "https://tiny.pl/9n2pn" } });
            mMovies.Add(new MovieModel() { Title = "Inglourious Basterds", Date = "2011", Image = "https://tiny.pl/9n27n", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Director = "Augustine Frizzell", Actors = new string[] { "Zendaya", "Hunter Schafer", "Angus Cloud" }, MovieName = new string[] { "Rue", "Jules", "Fezco" }, ActorsImage = new string[] { "https://tiny.pl/9n2jh", "https://tiny.pl/9n2jm", "https://tiny.pl/9n2pn" } });
            mMovies.Add(new MovieModel() { Title = "Friends", Date = "1994", Image = "https://tiny.pl/9n27p", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Director = "Augustine Frizzell", Actors = new string[] { "Zendaya", "Hunter Schafer", "Angus Cloud" }, MovieName = new string[] { "Rue", "Jules", "Fezco" }, ActorsImage = new string[] { "https://tiny.pl/9n2jh", "https://tiny.pl/9n2jm", "https://tiny.pl/9n2pn" } });
            mMovies.Add(new MovieModel() { Title = "Games of thrones", Date = "2011", Image = "https://tiny.pl/9n2rg", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Director = "Augustine Frizzell", Actors = new string[] { "Zendaya", "Hunter Schafer", "Angus Cloud" }, MovieName = new string[] { "Rue", "Jules", "Fezco" }, ActorsImage = new string[] { "https://tiny.pl/9n2jh", "https://tiny.pl/9n2jm", "https://tiny.pl/9n2pn" } });
            mMovies.Add(new MovieModel() { Title = "Games of thrones", Date = "2011", Image = "https://tiny.pl/9n2rg", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Director = "Augustine Frizzell", Actors = new string[] { "Zendaya", "Hunter Schafer", "Angus Cloud" }, MovieName = new string[] { "Rue", "Jules", "Fezco" }, ActorsImage = new string[] { "https://tiny.pl/9n2jh", "https://tiny.pl/9n2jm", "https://tiny.pl/9n2pn" } });
            mMovies.Add(new MovieModel() { Title = "Games of thrones", Date = "2011", Image = "https://tiny.pl/9n2rg", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Director = "Augustine Frizzell", Actors = new string[] { "Zendaya", "Hunter Schafer", "Angus Cloud" }, MovieName = new string[] { "Rue", "Jules", "Fezco" }, ActorsImage = new string[] { "https://tiny.pl/9n2jh", "https://tiny.pl/9n2jm", "https://tiny.pl/9n2pn" } });
            mMovies.Add(new MovieModel() { Title = "Games of thrones", Date = "2011", Image = "https://tiny.pl/9n2rg", Description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.", Director = "Augustine Frizzell", Actors = new string[] { "Zendaya", "Hunter Schafer", "Angus Cloud" }, MovieName = new string[] { "Rue", "Jules", "Fezco" }, ActorsImage = new string[] { "https://tiny.pl/9n2jh", "https://tiny.pl/9n2jm", "https://tiny.pl/9n2pn" } });

            return mMovies;
        }
    }
}
