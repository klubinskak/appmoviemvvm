﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MovieAppMVVM.Core.Model;

namespace MovieAppMVVM.Core.Service
{
    public interface IAddMovies
    {
        List<MovieModel> GetMovies();
    }
}
