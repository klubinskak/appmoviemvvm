﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MovieAppMVVM.Core.ViewModel;
using MvvmCross.Platforms.Android.Views;

namespace MovieAppMVVM.Android.View
{
    [Activity(Label = "Movies", MainLauncher = true)]
    public class MainAcitvityView : MvxActivity<MoviesViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.mvx_recycler_view);
        }

    }
}
