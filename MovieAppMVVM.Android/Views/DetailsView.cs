﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MovieAppMVVM.Core.ViewModel;
using MvvmCross.Platforms.Android.Views;

namespace MovieAppMVVM.Android.Views
{
    [Activity(Label = "DetailsView")]
    public class DetailsView : MvxActivity<DetailsViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.details_view);
        }
    }
}
