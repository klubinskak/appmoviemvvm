﻿using System;
using Android.App;
using Android.Runtime;
using MovieAppMVVM.Core;
using MvvmCross.Platforms.Android.Core;
using MvvmCross.Platforms.Android.Views;

namespace MovieAppMVVM.Android
{
     [Application]
     public class MainApplication : MvxAndroidApplication<MvxAndroidSetup<App>, App>
     {
            public MainApplication(IntPtr javaReference, JniHandleOwnership transfer)
            : base(javaReference, transfer)
        {
        }
     }
}
